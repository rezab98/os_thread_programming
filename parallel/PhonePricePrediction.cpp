#include <string>
#include <sstream>
#include <vector>
#include <fstream>
#include <iostream>
#include <cmath>
#include <utility>
#include <chrono>
#include<unistd.h>
#include <iomanip>
#define NUMBER_OF_THREADS 4
using namespace std;

long double weights[6][25], dataOne[50000][25], normalizedDataOne[50000][25],
            classesOne[50000], dataTwo[50000][25], normalizedDataTwo[50000][25],
            classesTwo[50000], dataThree[50000][25], normalizedDataThree[50000][25],
            classesThree[50000], dataFour[50000][25], normalizedDataFour[50000][25],
            classesFour[50000];
long double accuracies = 0;
int finalNumberOfRows = 0;

int ReadDataOne(string fileName){
    ifstream theFile (fileName);
    string line;
    int i = 0;
    while(getline(theFile, line)) {
        string lineValue;
        stringstream ss(line);
        int j = 0;
        while (getline(ss, lineValue, ',')) {
            if (i != 0){
                dataOne[i - 1][j] = stold(lineValue);
                j++;
            }
        }
        i++;
    }
    return i - 1;
}

int ReadDataTwo(string fileName){
    ifstream theFile (fileName);
    string line;
    int i = 0;
    while(getline(theFile, line)) {
        string lineValue;
        stringstream ss(line);
        int j = 0;
        while (getline(ss, lineValue, ',')) {
            if (i != 0){
                dataTwo[i - 1][j] = stold(lineValue);
                j++;
            }
        }
        i++;
    }
    return i - 1;
}

int ReadDataThree(string fileName){
    ifstream theFile (fileName);
    string line;
    int i = 0;
    while(getline(theFile, line)) {
        string lineValue;
        stringstream ss(line);
        int j = 0;
        while (getline(ss, lineValue, ',')) {
            if (i != 0){
                dataThree[i - 1][j] = stold(lineValue);
                j++;
            }
        }
        i++;
    }
    return i - 1;
}

int ReadDataFour(string fileName) {
    ifstream theFile(fileName);
    string line;
    int i = 0;
    while (getline(theFile, line)) {
        string lineValue;
        stringstream ss(line);
        int j = 0;
        while (getline(ss, lineValue, ',')) {
            if (i != 0) {
                dataFour[i - 1][j] = stold(lineValue);
                j++;
            }
        }
        i++;
    }
    return i - 1;
}



void NormalizeDataOne(int numberOfRows){
    long double min[25], max[25];
    fill_n(min, 20, 999999);
    fill_n(max, 20, 0);
    for(int i = 0; i < 20; i++)
        for(int j = 0 ; j < numberOfRows; j++){
            if(dataOne[j][i] < min[i])
                min[i] = dataOne[j][i];
            else if(dataOne[j][i] > max[i])
                max[i] = dataOne[j][i];
        }
    for(int i = 0; i < 20; i++)
        for(int j = 0 ; j < numberOfRows; j++){
            long double normalized = (dataOne[j][i] - min[i]) / (max[i] - min[i]);
            normalizedDataOne[j][i] = normalized;
        }
}

void NormalizeDataTwo(int numberOfRows){
    long double min[25], max[25];
    fill_n(min, 20, 999999);
    fill_n(max, 20, 0);
    for(int i = 0; i < 20; i++)
        for(int j = 0 ; j < numberOfRows; j++){
            if(dataTwo[j][i] < min[i])
                min[i] = dataTwo[j][i];
            else if(dataTwo[j][i] > max[i])
                max[i] = dataTwo[j][i];
        }
    for(int i = 0; i < 20; i++)
        for(int j = 0 ; j < numberOfRows; j++){
            long double normalized = (dataTwo[j][i] - min[i]) / (max[i] - min[i]);
            normalizedDataTwo[j][i] = normalized;
        }
}

void NormalizeDataThree(int numberOfRows){
    long double min[25], max[25];
    fill_n(min, 20, 999999);
    fill_n(max, 20, 0);
    for(int i = 0; i < 20; i++)
        for(int j = 0 ; j < numberOfRows; j++){
            if(dataThree[j][i] < min[i])
                min[i] = dataThree[j][i];
            else if(dataThree[j][i] > max[i])
                max[i] = dataThree[j][i];
        }
    for(int i = 0; i < 20; i++)
        for(int j = 0 ; j < numberOfRows; j++){
            long double normalized = (dataThree[j][i] - min[i]) / (max[i] - min[i]);
            normalizedDataThree[j][i] = normalized;
        }
}

void NormalizeDataFour(int numberOfRows){
    long double min[25], max[25];
    fill_n(min, 20, 999999);
    fill_n(max, 20, 0);
    for(int i = 0; i < 20; i++)
        for(int j = 0 ; j < numberOfRows; j++){
            if(dataFour[j][i] < min[i])
                min[i] = dataFour[j][i];
            else if(dataFour[j][i] > max[i])
                max[i] = dataFour[j][i];
        }
    for(int i = 0; i < 20; i++)
        for(int j = 0 ; j < numberOfRows; j++){
            long double normalized = (dataFour[j][i] - min[i]) / (max[i] - min[i]);
            normalizedDataFour[j][i] = normalized;
        }
}

void ReadWeights(string fileName) {
    ifstream theFile (fileName);
    string line;
    int i = 0;
    while(getline(theFile, line)) {
        string lineValue;
        stringstream ss(line);
        int j = 0;
        while (getline(ss, lineValue, ',')) {
            if (i != 0){
                weights[i - 1][j] = stold(lineValue);
                j++;
            }
        }
        i++;
    }
}

void ClassifyOne(int numberOfRows){
    for(int i = 0; i < numberOfRows; i++){
        long double scoreOne = weights[0][20];
        long double scoreTwo = weights[1][20];
        long double scoreThree = weights[2][20];
        long double scoreFour = weights[3][20];
        for(int j = 0; j < 20; j++) {
            scoreOne += (normalizedDataOne[i][j] * weights[0][j]);
            scoreTwo += (normalizedDataOne[i][j] * weights[1][j]);
            scoreThree += (normalizedDataOne[i][j] * weights[2][j]);
            scoreFour += (normalizedDataOne[i][j] * weights[3][j]);
        }
        long double scoreMax = max(max(scoreOne, scoreTwo), max(scoreThree, scoreFour));
        if(scoreMax == scoreOne)
            classesOne[i] = 0.0;
        else if(scoreMax == scoreTwo)
            classesOne[i] = 1.0;
        else if(scoreMax == scoreThree)
            classesOne[i] = 2.0;
        else
            classesOne[i] = 3.0;

    }
}


void ClassifyTwo(int numberOfRows){
    for(int i = 0; i < numberOfRows; i++){
        long double scoreOne = weights[0][20];
        long double scoreTwo = weights[1][20];
        long double scoreThree = weights[2][20];
        long double scoreFour = weights[3][20];
        for(int j = 0; j < 20; j++) {
            scoreOne += (normalizedDataTwo[i][j] * weights[0][j]);
            scoreTwo += (normalizedDataTwo[i][j] * weights[1][j]);
            scoreThree += (normalizedDataTwo[i][j] * weights[2][j]);
            scoreFour += (normalizedDataTwo[i][j] * weights[3][j]);
        }
        long double scoreMax = max(max(scoreOne, scoreTwo), max(scoreThree, scoreFour));
        if(scoreMax == scoreOne)
            classesTwo[i] = 0.0;
        else if(scoreMax == scoreTwo)
            classesTwo[i] = 1.0;
        else if(scoreMax == scoreThree)
            classesTwo[i] = 2.0;
        else
            classesTwo[i] = 3.0;

    }
}

void ClassifyThree(int numberOfRows){
    for(int i = 0; i < numberOfRows; i++){
        long double scoreOne = weights[0][20];
        long double scoreTwo = weights[1][20];
        long double scoreThree = weights[2][20];
        long double scoreFour = weights[3][20];
        for(int j = 0; j < 20; j++) {
            scoreOne += (normalizedDataThree[i][j] * weights[0][j]);
            scoreTwo += (normalizedDataThree[i][j] * weights[1][j]);
            scoreThree += (normalizedDataThree[i][j] * weights[2][j]);
            scoreFour += (normalizedDataThree[i][j] * weights[3][j]);
        }
        long double scoreMax = max(max(scoreOne, scoreTwo), max(scoreThree, scoreFour));
        if(scoreMax == scoreOne)
            classesThree[i] = 0.0;
        else if(scoreMax == scoreTwo)
            classesThree[i] = 1.0;
        else if(scoreMax == scoreThree)
            classesThree[i] = 2.0;
        else
            classesThree[i] = 3.0;

    }
}

void ClassifyFour(int numberOfRows){
    for(int i = 0; i < numberOfRows; i++){
        long double scoreOne = weights[0][20];
        long double scoreTwo = weights[1][20];
        long double scoreThree = weights[2][20];
        long double scoreFour = weights[3][20];
        for(int j = 0; j < 20; j++) {
            scoreOne += (normalizedDataFour[i][j] * weights[0][j]);
            scoreTwo += (normalizedDataFour[i][j] * weights[1][j]);
            scoreThree += (normalizedDataFour[i][j] * weights[2][j]);
            scoreFour += (normalizedDataFour[i][j] * weights[3][j]);
        }
        long double scoreMax = max(max(scoreOne, scoreTwo), max(scoreThree, scoreFour));
        if(scoreMax == scoreOne)
            classesFour[i] = 0.0;
        else if(scoreMax == scoreTwo)
            classesFour[i] = 1.0;
        else if(scoreMax == scoreThree)
            classesFour[i] = 2.0;
        else
            classesFour[i] = 3.0;

    }
}

long double FindAccuracyOne(int numberOfRows){
    int numberOfCorrectlyClassified = 0;
    for(int i = 0; i < numberOfRows; i++) {
        if (abs(classesOne[i] - dataOne[i][20]) < 1e-6)
            numberOfCorrectlyClassified++;
    }


    double accuracy = double(numberOfCorrectlyClassified) / double(numberOfRows);
    return accuracy;
}

long double FindAccuracyTwo(int numberOfRows){
    int numberOfCorrectlyClassified = 0;
    for(int i = 0; i < numberOfRows; i++)
        if(abs(classesTwo[i] - dataTwo[i][20]) < 1e-6)
            numberOfCorrectlyClassified++;

    double accuracy = double(numberOfCorrectlyClassified) / double(numberOfRows);
    return accuracy;
}

long double FindAccuracyThree(int numberOfRows){
    int numberOfCorrectlyClassified = 0;
    for(int i = 0; i < numberOfRows; i++)
        if(abs(classesThree[i] - dataThree[i][20]) < 1e-6)
            numberOfCorrectlyClassified++;

    double accuracy = double(numberOfCorrectlyClassified) / double(numberOfRows);
    return accuracy;
}

long double FindAccuracyFour(int numberOfRows){
    int numberOfCorrectlyClassified = 0;
    for(int i = 0; i < numberOfRows; i++)
        if(abs(classesFour[i] - dataFour[i][20]) < 1e-6)
            numberOfCorrectlyClassified++;

    double accuracy = double(numberOfCorrectlyClassified) / double(numberOfRows);
    return accuracy;
}

long double FindFinalAccuracy(){

    long double finalAccuracy = accuracies / finalNumberOfRows;
    return finalAccuracy;
}


void* RoutineOne(void* args){
    int numberOfRows = ReadDataOne("train_0.csv");
    NormalizeDataOne(numberOfRows);
    ClassifyOne(numberOfRows);
    double accuracy = FindAccuracyOne(numberOfRows);
    accuracies += (numberOfRows * accuracy);
    finalNumberOfRows += numberOfRows;



}

void* RoutineTwo(void* args){
    int numberOfRows = ReadDataTwo("train_1.csv");
    NormalizeDataTwo(numberOfRows);
    ClassifyTwo(numberOfRows);
    long double accuracy = FindAccuracyTwo(numberOfRows);
    accuracies += (numberOfRows * accuracy);
    finalNumberOfRows += numberOfRows;



}

void* RoutineThree(void* args){
    int numberOfRows = ReadDataThree("train_2.csv");
    NormalizeDataThree(numberOfRows);
    ClassifyThree(numberOfRows);
    double accuracy = FindAccuracyThree(numberOfRows);
    accuracies += (numberOfRows * accuracy);
    finalNumberOfRows += numberOfRows;



}

void* RoutineFour(void* args){
    int numberOfRows = ReadDataFour("train_3.csv");
    NormalizeDataFour(numberOfRows);
    ClassifyFour(numberOfRows);
    double accuracy = FindAccuracyFour(numberOfRows);
    accuracies += (numberOfRows * accuracy);
    finalNumberOfRows += numberOfRows;

}

int main(int argc,char* argv[]) {

   // auto t1 = std::chrono::high_resolution_clock::now();
    chdir("..");chdir(argv[1]);
    ReadWeights("weights.csv");
    pthread_t threads[NUMBER_OF_THREADS];
    for(int tid = 0; tid < NUMBER_OF_THREADS; tid++){
        int return_code;
        if(tid == 0)
            return_code = pthread_create(&threads[tid],
                                         NULL, RoutineOne, NULL);
        else if(tid == 1)
            return_code = pthread_create(&threads[tid],
                                         NULL, RoutineTwo, NULL);
        else if(tid == 2)
            return_code = pthread_create(&threads[tid],
                                         NULL, RoutineThree, NULL);
        else if(tid == 3)
            return_code = pthread_create(&threads[tid],
                                         NULL, RoutineFour, NULL);

        if (return_code){
            printf("ERROR; return code from pthread_create() is %d\n",
                   return_code);
            exit(-1);
        }
    }

    pthread_join(threads[0], NULL);
    pthread_join(threads[1], NULL);
    pthread_join(threads[2], NULL);
    pthread_join(threads[3], NULL);
    long double accuracy = FindFinalAccuracy() * 100;
    cout.precision(2);
    cout << fixed << accuracy;
    cout << "%";
    //auto t2 = std::chrono::high_resolution_clock::now();
    //auto duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
    //cout << duration;
}










































