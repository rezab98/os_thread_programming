#include <string>
#include <sstream>
#include <vector>
#include <fstream>
#include <iostream>
#include <cmath>
#include <chrono>
#include<unistd.h>
#include <iomanip>
using namespace std;

long double data[500000][25], normalizedData[500000][25], weights[6][25], classes[500000];

int ReadData(string fileName){
    ifstream theFile (fileName);
    string line;
    int i = 0;
    while(getline(theFile, line)) {
        string lineValue;
        stringstream ss(line);
        int j = 0;
        while (getline(ss, lineValue, ',')) {
            if (i != 0){
                data[i - 1][j] = stold(lineValue);
                j++;
            }
        }
        i++;
    }
    return i - 1;
}



void NormalizeData(int numberOfRows){
    long double min[25], max[25];
    fill_n(min, 20, 999999);
    fill_n(max, 20, 0);
    for(int i = 0; i < 20; i++)
        for(int j = 0 ; j < numberOfRows; j++){
            if(data[j][i] < min[i])
                min[i] = data[j][i];
            else if(data[j][i] > max[i])
                max[i] = data[j][i];
        }
    for(int i = 0; i < 20; i++)
        for(int j = 0 ; j < numberOfRows; j++){
            long double normalized = (data[j][i] - min[i]) / (max[i] - min[i]);
            normalizedData[j][i] = normalized;
        }
}

void ReadWeights(string fileName) {
    ifstream theFile (fileName);
    string line;
    int i = 0;
    while(getline(theFile, line)) {
        string lineValue;
        stringstream ss(line);
        int j = 0;
        while (getline(ss, lineValue, ',')) {
            if (i != 0){
                weights[i - 1][j] = stold(lineValue);
                j++;
            }
        }
        i++;
    }
}

void Classify(int numberOfRows){
    for(int i = 0; i < numberOfRows; i++){
        long double scoreOne = weights[0][20];
        long double scoreTwo = weights[1][20];
        long double scoreThree = weights[2][20];
        long double scoreFour = weights[3][20];
        for(int j = 0; j < 20; j++) {
            scoreOne += (normalizedData[i][j] * weights[0][j]);
            scoreTwo += (normalizedData[i][j] * weights[1][j]);
            scoreThree += (normalizedData[i][j] * weights[2][j]);
            scoreFour += (normalizedData[i][j] * weights[3][j]);
        }
        long double scoreMax = max(max(scoreOne, scoreTwo), max(scoreThree, scoreFour));
        if(scoreMax == scoreOne)
            classes[i] = 0.0;
        else if(scoreMax == scoreTwo)
            classes[i] = 1.0;
        else if(scoreMax == scoreThree)
            classes[i] = 2.0;
        else
            classes[i] = 3.0;

    }
}

long double findAccuracy(int numberOfRows){
    int numberOfCorrectlyClassified = 0;
    for(int i = 0; i < numberOfRows; i++){
        if(abs(classes[i] - data[i][20]) < 1e-6)
            numberOfCorrectlyClassified++;
    }
    double accuracy = double(numberOfCorrectlyClassified) / double(numberOfRows);
    return accuracy;
}
int main(int argc,char* argv[]){
    //auto t1 = std::chrono::high_resolution_clock::now();
    chdir("..");chdir(argv[1]);
    int numberOfRows = ReadData("train.csv");
    NormalizeData(numberOfRows);
    ReadWeights("weights.csv");
    Classify(numberOfRows);
    double accuracy = findAccuracy(numberOfRows) * 100;
    cout.precision(2);
    cout << fixed << accuracy;
    cout << "%";
    //auto t2 = std::chrono::high_resolution_clock::now();
    //auto duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
    //cout << endl << duration;

}









































